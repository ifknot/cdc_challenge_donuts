import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true
    width: 400
    height: 400

    Rectangle {
        id: donut
        width: parent.width<parent.height?parent.width:parent.height
        height: width
        gradient: Gradient {
            GradientStop { position: 0.0; color: "plum" }
            GradientStop { position: 0.75; color: "pink" }
        }
        border.color: "black"
        border.width: 6
        radius: width*0.5
    }

    Rectangle {
        id: hole
        anchors.centerIn: parent
        width: donut.width / 2.5
        height: width
        color: "white"
        border.color: "black"
        border.width: 6
        radius: width*0.5
    }



}
